package com.yomahub.demo.proxy.vo;

public interface Animal {

    void wakeup();

    void sleep();
}
