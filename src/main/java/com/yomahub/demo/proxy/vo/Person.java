package com.yomahub.demo.proxy.vo;

public interface Person {

    void wakeup();

    void sleep();
}
