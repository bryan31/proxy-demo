package com.yomahub.demo.proxy.statics;

import com.yomahub.demo.proxy.vo.Animal;

public class AnimalProxy implements Animal {

    private Animal animal;

    public AnimalProxy(Animal animal) {
        this.animal = animal;
    }

    @Override
    public void wakeup() {
        System.out.println("早安~~");
        animal.wakeup();
    }

    @Override
    public void sleep() {
        System.out.println("晚安~~");
        animal.sleep();
    }
}
